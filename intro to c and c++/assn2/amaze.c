#include <stdio.h>
#include <stdbool.h>

// Check if int is odd
bool is_odd(int i) {
	return i % 2 != 0;
}

// Print leader
void amaze(int i) {
	printf("amaze%d:\t", i);
}

// Print number with space: '1 '
void sp(int i) {
	printf("%d ", i);
}

// Print new line
void nl() {
	printf("\n");
}

// for loop
void amaze1() {
	int i;
	amaze(1);
	for (i = 0; i <= 10; i++) {
		if (i % 2 == 1) {
			sp(i);
		}
	}
	nl();
}

// goto
void amaze2() {
	amaze(2);
	int i = 0;
printer:
	if (is_odd(i)) {
		sp(i);
	}
	i++;

	if (i <= 10) {
		goto printer;
	}

	nl();
}

// do while
void amaze3() {
	amaze(3);
	int i = 0;
	do {
		if (is_odd(i)) {
			sp(i);
		}
		i++;
	} while (i < 10);
	nl();
}

// while
void amaze4() {
	amaze(4);
	int i = 0;
	while (i < 10) {
		if (is_odd(i)) {
			sp(i);
		}
		i++;
	}
	nl();
}

// odd array
void amaze5() {
	amaze(5);
	int arr[] = {1, 3, 5, 7, 9,};
	int len = sizeof(arr) / sizeof(arr[0]);
	for (int i=0; i<len; i++)  {
		sp(arr[i]);
	}
	nl();
}

// switch
void amaze6() {
	amaze(6);
	for (int i=0; i<10; i++) {
		switch (i % 2) {
			case 1:
				sp(i);
				break;
			default:
				break;
		}
	}
	nl();
}

// ridiculous switch
void amaze7() {
	amaze(7);
	for (int i=0; i<10; i++) {
		switch (i) {
			case 1: case 3: case 5: case 7: case 9:
				sp(i);
				break;
			default:
				break;
		}
	}
	nl();
}

int main() {
	amaze1();
	amaze2();
	amaze3();
	amaze4();
	amaze5();
	amaze6();
	amaze7();
	return 0;
}
