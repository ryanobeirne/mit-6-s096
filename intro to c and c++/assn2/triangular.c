#include <stdio.h>

int triangular(int n) {
	int t = 0;

	int i;
	for (i=0; i<=n; i++) {
		t += i;
	}

	return t;
}

int main() {
	int i;
	for (i=0; i<100; i++) {
		int t = triangular(i);
		printf("%d, ", t);
	}

	printf("\n");

	return 0;
}
