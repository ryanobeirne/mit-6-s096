#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() { 	
    char hakuna[] = "hakuna matata!";
    char * str = (char *) malloc(sizeof(hakuna));
    strcpy(str, hakuna); // this line should copy "hakuna matata!"
                         // into our char array

    printf("%s\n", str); // Anything else?

    free(str);
}
 
